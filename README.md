# README #

### What is this repository for? ###

angular-ui project is intended to be a seed for new projects. It can also be used as a tutorial for learning the basics for angular and ui-router configuration
 
* Version 1.0

### How do I get set up? ###

Additional configuration is not needed to get the project running out of the box.  
  
Dependencies included:  
- Angular 1.4.7 (uncompressed)  
- UI-Router 0.2.15  
- 3 generic views (Html files)  
- 3 generic controllers  
- 3 generic states (see router.js)  

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Eric Ruiz - ericruiz.developer@gmail.com