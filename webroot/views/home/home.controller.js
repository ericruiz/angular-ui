/**
 * Created by eruiz on 11/11/15.
 */
(function () {
    'use strict';

    angular.module('myApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$state', '$scope'];
    function HomeController($state, $scope) {
        var vm = this;

        vm.greeting = "Home Page";
    }
})();