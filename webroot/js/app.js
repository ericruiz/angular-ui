/**
 * Created by eruiz on 11/11/15.
 */
(function() {
    'use strict';

    angular.module('myApp', [
        'ui.router'
    ])
        .controller('ApplicationController', ApplicationController);

    ApplicationController.$inject = ['$scope', '$location', '$rootScope'];

    function ApplicationController($scope, $location, $rootScope) {
        $scope.$on('$stateChangeSuccess', onStateChangeSuccess);

        function onStateChangeSuccess(event, toState, toParams, fromState, fromParams) {

        }
    }
})();