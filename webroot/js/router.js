/**
 * Created by eruiz on 11/11/2015
 */
(function() {
    'use strict';

    angular.module('myApp')
        .config(config);

    function config($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('home');

        $stateProvider
            .state('home', {
                url: '/home',
                templateUrl: 'views/home/home.html',
                controller: 'HomeController as vm'
            })
            .state('about', {
                url: '/about',
                templateUrl: 'views/about/about.html',
                controller: 'AboutController as vm'
            })
            .state('contact-us', {
                url: '/contact-us',
                templateUrl: 'views/contact-us/contact-us.html',
                controller: 'ContactUsController as vm'
            })

    }
})();
